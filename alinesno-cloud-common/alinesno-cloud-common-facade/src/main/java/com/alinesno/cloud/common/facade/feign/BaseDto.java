package com.alinesno.cloud.common.facade.feign;

import java.io.Serializable;
import java.util.Date;

import com.alinesno.cloud.common.facade.date.CustomJsonDateDeserializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * TODO 待找方案
 * 对象实体之间的传输，没想到更好的办法，其中BaseVo与BaseEntity属性对应，为了在两个服务之间做数据传输与调用使用，这样也会导致同时需要维护两个实体对象的困难
 * 
 * @author LuoAnDong
 * @since 2018年11月22日 上午9:54:44
 */
@SuppressWarnings("serial")
public class BaseDto implements Serializable {

	private String id; // 唯一ID号
	private String fieldProp; // 字段属性

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonDeserialize(using = CustomJsonDateDeserializer.class)
	private Date addTime; // 添加时间

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonDeserialize(using = CustomJsonDateDeserializer.class)
	private Date deleteTime; // 删除时间

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonDeserialize(using = CustomJsonDateDeserializer.class)
	private Date updateTime; // 更新时间

	private int hasDelete = 0; // 是否删除(1删除|0正常|null正常)
	private int hasStatus = 0; // 状态(0启用|1禁用)
	private String deleteManager; // 删除的人
	private String applicationId; // 所属应用
	private String tenantId; // 所属租户

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFieldProp() {
		return fieldProp;
	}

	public void setFieldProp(String fieldProp) {
		this.fieldProp = fieldProp;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	public Date getDeleteTime() {
		return deleteTime;
	}

	public void setDeleteTime(Date deleteTime) {
		this.deleteTime = deleteTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public int getHasDelete() {
		return hasDelete;
	}

	public void setHasDelete(int hasDelete) {
		this.hasDelete = hasDelete;
	}

	public int getHasStatus() {
		return hasStatus;
	}

	public void setHasStatus(int hasStatus) {
		this.hasStatus = hasStatus;
	}

	public String getDeleteManager() {
		return deleteManager;
	}

	public void setDeleteManager(String deleteManager) {
		this.deleteManager = deleteManager;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

}
