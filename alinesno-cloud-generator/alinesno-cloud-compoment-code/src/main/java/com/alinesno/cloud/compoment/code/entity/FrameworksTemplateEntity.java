package com.alinesno.cloud.compoment.code.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 框架配置文件模板
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Entity
@Table(name="frameworks_template")
public class FrameworksTemplateEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 模板编码
     */
	private String code;
    /**
     * 框架编码
     */
	private String frameworksCode;
    /**
     * 模板路径
     */
	private String path;


	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getFrameworksCode() {
		return frameworksCode;
	}

	public void setFrameworksCode(String frameworksCode) {
		this.frameworksCode = frameworksCode;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}


	@Override
	public String toString() {
		return "FrameworksTemplateEntity{" +
			"code=" + code +
			", frameworksCode=" + frameworksCode +
			", path=" + path +
			"}";
	}
}
