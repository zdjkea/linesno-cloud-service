package com.alinesno.cloud.compoment.code.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.bean.DatatablesPageBean;
import com.alinesno.cloud.common.web.base.controller.LocalMethodBaseController;
import com.alinesno.cloud.compoment.code.entity.ProjectJobLogsEntity;
import com.alinesno.cloud.compoment.code.service.IProjectJobLogsService;

/**
 * <p>任务日志 前端控制器 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("compoment/code/projectJobLogs")
public class ProjectJobLogsController extends LocalMethodBaseController<ProjectJobLogsEntity, IProjectJobLogsService> {

	//日志记录
	private static final Logger log = LoggerFactory.getLogger(ProjectJobLogsController.class);

	@TranslateCode(value="[{hasStatus:has_status}]")
	@ResponseBody
	@PostMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, feign , page) ;
    }


}


























