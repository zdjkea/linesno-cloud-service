package com.alinesno.cloud.demo.base.rest;

import static org.apache.commons.lang.RandomStringUtils.randomNumeric;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alinesno.cloud.common.core.rest.BaseRestController;
import com.alinesno.cloud.demo.base.entity.LoggerEntity;
import com.alinesno.cloud.demo.base.services.ILoggerService;

@RestController
@RequestMapping("hello")
public class HelloController  extends BaseRestController<LoggerEntity , ILoggerService> {

	private static final Logger log = LoggerFactory.getLogger(HelloController.class);

	@Value("${timeout:100}")
	private int timeout;

	@Autowired
	private ILoggerService loggerService ;

	@RequestMapping(method = RequestMethod.GET, value = "/hello/{name}")
	public String hello(@PathVariable String name) {
		String returnVal = randomNumeric(2) + name + " , timeout = " + timeout + ", all = " + loggerService.count();

		log.debug("return value = {}", returnVal);

		return returnVal;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/hello")
	public String world(@RequestParam String name) {
		return name + "success";
	}
}