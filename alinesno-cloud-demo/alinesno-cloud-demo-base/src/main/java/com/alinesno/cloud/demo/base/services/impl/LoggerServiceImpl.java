package com.alinesno.cloud.demo.base.services.impl;

import org.springframework.stereotype.Service;

import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;
import com.alinesno.cloud.demo.base.entity.LoggerEntity;
import com.alinesno.cloud.demo.base.repository.LoggerRepository;
import com.alinesno.cloud.demo.base.services.ILoggerService;

@Service
public class LoggerServiceImpl extends IBaseServiceImpl<LoggerRepository, LoggerEntity, String> implements ILoggerService {


}
