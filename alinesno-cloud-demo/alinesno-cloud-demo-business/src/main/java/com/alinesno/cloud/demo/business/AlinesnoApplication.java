package com.alinesno.cloud.demo.business;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 启动入口
 * 
 * @author LuoAnDong
 * @since 2018年8月7日 上午8:45:02
 */
@SpringBootApplication
@EnableAsync // 开启异步任务
@EnableEurekaClient
@EnableSwagger2
@EnableFeignClients(basePackages = "com.alinesno.cloud.**.feign.facade.**") // 扫描feign包下的，变成接口可调用包
public class AlinesnoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlinesnoApplication.class, args);
	}

}
