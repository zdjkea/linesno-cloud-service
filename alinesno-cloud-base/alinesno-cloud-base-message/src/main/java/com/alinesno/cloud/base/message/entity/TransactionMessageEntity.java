package com.alinesno.cloud.base.message.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-08 09:28:52
 */
@Entity
@Table(name="transaction_message")
public class TransactionMessageEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 是否死亡
     */
	@Column(name="areadly_dead")
	private Integer areadlyDead;
    /**
     * 消息内容
     */
	@Column(name="business_body")
	private String businessBody;
    /**
     * 消息数据类型
     */
	@Column(name="business_date_type")
	private String businessDateType;
    /**
     * 业务主键
     */
	@Column(name="business_id")
	private String businessId;
    /**
     * 创建时间
     */
	@Column(name="create_time")
	private Date createTime;
    /**
     * 创建人
     */
	private String creater;
    /**
     * 编辑时间
     */
	@Column(name="edit_time")
	private Date editTime;
    /**
     * 编辑人
     */
	private String editor;
    /**
     * 扩展字段1
     */
	private String field1;
    /**
     * 扩展字段2
     */
	private String field2;
    /**
     * 扩展字段3
     */
	private String field3;
    /**
     * 消息发送时间
     */
	@Column(name="message_send_times")
	private Integer messageSendTimes;
    /**
     * 消息状态
     */
	@Column(name="message_status")
	private String messageStatus;
    /**
     * 消息备注
     */
	private String remark;
    /**
     * 消息主题
     */
	private String topic;
    /**
     * 消息版本
     */
	private Integer versions;


	public Integer getAreadlyDead() {
		return areadlyDead;
	}

	public void setAreadlyDead(Integer areadlyDead) {
		this.areadlyDead = areadlyDead;
	}

	public String getBusinessBody() {
		return businessBody;
	}

	public void setBusinessBody(String businessBody) {
		this.businessBody = businessBody;
	}

	public String getBusinessDateType() {
		return businessDateType;
	}

	public void setBusinessDateType(String businessDateType) {
		this.businessDateType = businessDateType;
	}

	public String getBusinessId() {
		return businessId;
	}

	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}

	public Date getEditTime() {
		return editTime;
	}

	public void setEditTime(Date editTime) {
		this.editTime = editTime;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public String getField1() {
		return field1;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public String getField2() {
		return field2;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public String getField3() {
		return field3;
	}

	public void setField3(String field3) {
		this.field3 = field3;
	}

	public Integer getMessageSendTimes() {
		return messageSendTimes;
	}

	public void setMessageSendTimes(Integer messageSendTimes) {
		this.messageSendTimes = messageSendTimes;
	}

	public String getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public Integer getVersions() {
		return versions;
	}

	public void setVersions(Integer versions) {
		this.versions = versions;
	}


	@Override
	public String toString() {
		return "TransactionMessageEntity{" +
			"areadlyDead=" + areadlyDead +
			", businessBody=" + businessBody +
			", businessDateType=" + businessDateType +
			", businessId=" + businessId +
			", createTime=" + createTime +
			", creater=" + creater +
			", editTime=" + editTime +
			", editor=" + editor +
			", field1=" + field1 +
			", field2=" + field2 +
			", field3=" + field3 +
			", messageSendTimes=" + messageSendTimes +
			", messageStatus=" + messageStatus +
			", remark=" + remark +
			", topic=" + topic +
			", versions=" + versions +
			"}";
	}
}
