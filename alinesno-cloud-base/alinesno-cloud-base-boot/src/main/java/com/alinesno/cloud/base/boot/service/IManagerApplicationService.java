package com.alinesno.cloud.base.boot.service;

import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.boot.entity.ManagerApplicationEntity;
import com.alinesno.cloud.base.boot.repository.ManagerApplicationRepository;
import com.alinesno.cloud.common.core.services.IBaseService;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@NoRepositoryBean
public interface IManagerApplicationService extends IBaseService<ManagerApplicationRepository, ManagerApplicationEntity, String> {

	/**
	 * 查询用户应用
	 * @param accountId
	 * @return
	 */
	List<ManagerApplicationEntity> findAllByAccountId(String accountId);

}
