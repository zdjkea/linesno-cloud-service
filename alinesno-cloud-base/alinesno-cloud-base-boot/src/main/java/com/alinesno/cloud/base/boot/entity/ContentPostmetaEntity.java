package com.alinesno.cloud.base.boot.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Entity
@Table(name="content_postmeta")
public class ContentPostmetaEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 所属文章
     */
	@Column(name="post_id")
	private Long postId;
    /**
     * 元素key
     */
	@Column(name="meta_key")
	private String metaKey;
    /**
     * 元素值
     */
	@Column(name="meta_value")
	private String metaValue;


	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

	public String getMetaKey() {
		return metaKey;
	}

	public void setMetaKey(String metaKey) {
		this.metaKey = metaKey;
	}

	public String getMetaValue() {
		return metaValue;
	}

	public void setMetaValue(String metaValue) {
		this.metaValue = metaValue;
	}


	@Override
	public String toString() {
		return "ContentPostmetaEntity{" +
			"postId=" + postId +
			", metaKey=" + metaKey +
			", metaValue=" + metaValue +
			"}";
	}
}
