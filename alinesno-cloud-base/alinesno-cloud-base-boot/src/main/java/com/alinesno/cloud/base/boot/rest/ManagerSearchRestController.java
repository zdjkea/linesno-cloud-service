package com.alinesno.cloud.base.boot.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alinesno.cloud.base.boot.entity.ManagerSearchEntity;
import com.alinesno.cloud.base.boot.service.IManagerSearchService;
import com.alinesno.cloud.common.core.rest.BaseRestController;

/**
 * <p> 接口 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Scope("prototype")
@RestController
@RequestMapping("managerSearch")
public class ManagerSearchRestController extends BaseRestController<ManagerSearchEntity , IManagerSearchService> {

	//日志记录
	@SuppressWarnings("unused")
	private final static Logger logger = LoggerFactory.getLogger(ManagerSearchRestController.class);

}
