package com.alinesno.cloud.base.boot.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alinesno.cloud.base.boot.entity.ManagerResourceEntity;
import com.alinesno.cloud.base.boot.service.IManagerResourceService;
import com.alinesno.cloud.common.core.rest.BaseRestController;

/**
 * <p> 接口 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Scope("prototype")
@RestController
@RequestMapping("managerResource")
public class ManagerResourceRestController extends BaseRestController<ManagerResourceEntity , IManagerResourceService> {

	//日志记录
	@SuppressWarnings("unused")
	private final static Logger logger = LoggerFactory.getLogger(ManagerResourceRestController.class);
	
	/**
	 * 查询出一级菜单 
	 * @param resourceParent
	 * @return
	 */
	@GetMapping("findMenus")
	ManagerResourceEntity findMenus(String resourceParent) {
		return feign.findMenus(resourceParent , null) ; 
	}

	/**
	 * 查询菜单
	 * @param resourceParent
	 * @param applicationId
	 * @return
	 */
	@GetMapping("findMenusByApplication")
	ManagerResourceEntity findMenusByApplication(String resourceParent , String applicationId) {
		return feign.findMenus(resourceParent , applicationId) ; 
	}

	/**
	 * 查询账号菜单 
	 * @param resourceParent
	 * @param applicationId
	 * @param accountId
	 * @return
	 */
	@GetMapping("findMenusByApplicationAndAccount")
	ManagerResourceEntity findMenusByApplicationAndAccount(String resourceParent, String applicationId, String accountId) {
		return feign.findMenus(resourceParent , applicationId , accountId) ; 
	}
}
