package com.alinesno.cloud.base.boot.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Entity
@Table(name="manager_application")
public class ManagerApplicationEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 应用名称
     */
	@Column(name="application_name")
	private String applicationName ;
	
    /**
     * 应用描述
     */
	@Column(name="application_desc")
	private String applicationDesc;
    /**
     * 应用图标 
     */
	@Column(name="application_icons")
	private String applicationIcons;

	/**
	 * 应用链接
	 */
	@Column(name="application_link")
	private String applicationLink ; 
	
    /**
     * 父类id
     */
	private String pid;

	public String getApplicationLink() {
		return applicationLink;
	}

	public void setApplicationLink(String applicationLink) {
		this.applicationLink = applicationLink;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getApplicationDesc() {
		return applicationDesc;
	}

	public void setApplicationDesc(String applicationDesc) {
		this.applicationDesc = applicationDesc;
	}

	public String getApplicationIcons() {
		return applicationIcons;
	}

	public void setApplicationIcons(String applicationIcons) {
		this.applicationIcons = applicationIcons;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}


	@Override
	public String toString() {
		return "ManagerApplicationEntity{" +
			", applicationDesc=" + applicationDesc +
			", applicationIcons=" + applicationIcons +
			", pid=" + pid +
			"}";
	}
}
