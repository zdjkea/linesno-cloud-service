package com.alinesno.cloud.base.logger.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.logger.entity.LogLoginEntity;
import com.alinesno.cloud.base.logger.repository.LogLoginRepository;
import com.alinesno.cloud.common.core.services.IBaseService;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:16:06
 */
@NoRepositoryBean
public interface ILogLoginService extends IBaseService<LogLoginRepository, LogLoginEntity, String> {

}
