package com.alinesno.cloud.base.notice.entity;

import javax.persistence.Column;
import java.util.Date;
import javax.persistence.Table;
import javax.persistence.Entity;
import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-08 07:19:08
 */
@Entity
@Table(name="email_send")
public class EmailSendEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

	@Column(name="email_content")
	private String emailContent;
	@Column(name="email_receiver")
	private String emailReceiver;
	@Column(name="email_send_count")
	private Integer emailSendCount;
	@Column(name="email_send_time")
	private Date emailSendTime;
	@Column(name="email_sender")
	private String emailSender;
	@Column(name="email_subject")
	private String emailSubject;
	@Column(name="email_type")
	private String emailType;


	public String getEmailContent() {
		return emailContent;
	}

	public void setEmailContent(String emailContent) {
		this.emailContent = emailContent;
	}

	public String getEmailReceiver() {
		return emailReceiver;
	}

	public void setEmailReceiver(String emailReceiver) {
		this.emailReceiver = emailReceiver;
	}

	public Integer getEmailSendCount() {
		return emailSendCount;
	}

	public void setEmailSendCount(Integer emailSendCount) {
		this.emailSendCount = emailSendCount;
	}

	public Date getEmailSendTime() {
		return emailSendTime;
	}

	public void setEmailSendTime(Date emailSendTime) {
		this.emailSendTime = emailSendTime;
	}

	public String getEmailSender() {
		return emailSender;
	}

	public void setEmailSender(String emailSender) {
		this.emailSender = emailSender;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getEmailType() {
		return emailType;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}


	@Override
	public String toString() {
		return "EmailSendEntity{" +
			"emailContent=" + emailContent +
			", emailReceiver=" + emailReceiver +
			", emailSendCount=" + emailSendCount +
			", emailSendTime=" + emailSendTime +
			", emailSender=" + emailSender +
			", emailSubject=" + emailSubject +
			", emailType=" + emailType +
			"}";
	}
}
