package com.alinesno.cloud.base.workflow.bean;

import java.io.Serializable;

public class ActivityInstEntity implements Serializable {
	private static final long serialVersionUID = -7501063523645844415L;
	/**
	 * 活动实例ID
	 */
	long activityInstID = -9223372036854775807L;
	/**
	 * 活动实例名称
	 */
	String activityInstName = null;
	/**
	 * 活动实例描述
	 */
	String activityInstDesc = null;
	/**
	 * 所属活动定义类型
	 */
	String activityType = null;
	/**
	 * 活动实例当前状态
	 */
	int currentState = -2147483647;
	/**
	 * 优先级
	 */
	int priority = -2147483647;
	/**
	 * 创建时间
	 */
	String createTime = null;
	/**
	 * 活动开始时间
	 */
	String startTime = null;
	/**
	 * 活动结束时间
	 */
	String endTime = null;
	/**
	 * 子业务流程ID
	 */
	long subProcessID = -9223372036854775807L;
	/**
	 * 所属活动定义ID
	 */
	String activityDefID = null;
	/**
	 * 所属的流程实例ID
	 */
	long processInstID = -9223372036854775807L;
	/**
	 * 回退标志
	 */
	String rollbackFlag = null;
	/**
	 * 
	 */
	String catalogUUID = null;
	/**
	 * 
	 */
	String catalogName = null;

	public long getActivityInstID() {
		return activityInstID;
	}

	public void setActivityInstID(long activityInstID) {
		this.activityInstID = activityInstID;
	}

	public String getActivityInstName() {
		return activityInstName;
	}

	public void setActivityInstName(String activityInstName) {
		this.activityInstName = activityInstName;
	}

	public String getActivityInstDesc() {
		return activityInstDesc;
	}

	public void setActivityInstDesc(String activityInstDesc) {
		this.activityInstDesc = activityInstDesc;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	public int getCurrentState() {
		return currentState;
	}

	public void setCurrentState(int currentState) {
		this.currentState = currentState;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public long getSubProcessID() {
		return subProcessID;
	}

	public void setSubProcessID(long subProcessID) {
		this.subProcessID = subProcessID;
	}

	public String getActivityDefID() {
		return activityDefID;
	}

	public void setActivityDefID(String activityDefID) {
		this.activityDefID = activityDefID;
	}

	public long getProcessInstID() {
		return processInstID;
	}

	public void setProcessInstID(long processInstID) {
		this.processInstID = processInstID;
	}

	public String getRollbackFlag() {
		return rollbackFlag;
	}

	public void setRollbackFlag(String rollbackFlag) {
		this.rollbackFlag = rollbackFlag;
	}

	public String getCatalogUUID() {
		return catalogUUID;
	}

	public void setCatalogUUID(String catalogUUID) {
		this.catalogUUID = catalogUUID;
	}

	public String getCatalogName() {
		return catalogName;
	}

	public void setCatalogName(String catalogName) {
		this.catalogName = catalogName;
	}

	@Override
	public String toString() {
		return "WFActivityInstEntity [activityInstID=" + activityInstID + ", activityInstName=" + activityInstName
				+ ", activityInstDesc=" + activityInstDesc + ", activityType=" + activityType + ", currentState="
				+ currentState + ", priority=" + priority + ", createTime=" + createTime + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", subProcessID=" + subProcessID + ", activityDefID=" + activityDefID
				+ ", processInstID=" + processInstID + ", rollbackFlag=" + rollbackFlag + ", catalogUUID=" + catalogUUID
				+ ", catalogName=" + catalogName + "]";
	}

}
