package com.alinesno.cloud.base.boot.feign.dto;

import java.util.Date;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:00:40
 */
@SuppressWarnings("serial")
public class ContentPostsDto extends BaseDto {

    /**
     * 文章作者
     */
	private String postAuthor;
	
    /**
     * 文章日期
     */
	private Date postDate;
	
    /**
     * 文章内容 
     */
	private String postContent;
	
    /**
     * 文章标题
     */
	private String postTitle;
	
    /**
     * 当前状态(1草稿/2已发布)
     */
	private Integer postStatus;
	
    /**
     * 文章访问密码
     */
	private String postPassword;
	
    /**
     * 文章名称
     */
	private String postName;
	
    /**
     * 文章最后修改时间 
     */
	private Date postModifield;
	
    /**
     * 文章类型
     */
	private String postType;
	
    /**
     * 文章分类
     */
	private String postMimeType;
	
    /**
     * 评论次数
     */
	private Integer commentCount;
	
    /**
     * 转发
     */
	private String toPing;
	


	public String getPostAuthor() {
		return postAuthor;
	}

	public void setPostAuthor(String postAuthor) {
		this.postAuthor = postAuthor;
	}

	public Date getPostDate() {
		return postDate;
	}

	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}

	public String getPostContent() {
		return postContent;
	}

	public void setPostContent(String postContent) {
		this.postContent = postContent;
	}

	public String getPostTitle() {
		return postTitle;
	}

	public void setPostTitle(String postTitle) {
		this.postTitle = postTitle;
	}

	public Integer getPostStatus() {
		return postStatus;
	}

	public void setPostStatus(Integer postStatus) {
		this.postStatus = postStatus;
	}

	public String getPostPassword() {
		return postPassword;
	}

	public void setPostPassword(String postPassword) {
		this.postPassword = postPassword;
	}

	public String getPostName() {
		return postName;
	}

	public void setPostName(String postName) {
		this.postName = postName;
	}

	public Date getPostModifield() {
		return postModifield;
	}

	public void setPostModifield(Date postModifield) {
		this.postModifield = postModifield;
	}

	public String getPostType() {
		return postType;
	}

	public void setPostType(String postType) {
		this.postType = postType;
	}

	public String getPostMimeType() {
		return postMimeType;
	}

	public void setPostMimeType(String postMimeType) {
		this.postMimeType = postMimeType;
	}

	public Integer getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(Integer commentCount) {
		this.commentCount = commentCount;
	}

	public String getToPing() {
		return toPing;
	}

	public void setToPing(String toPing) {
		this.toPing = toPing;
	}

}
