package com.alinesno.cloud.base.boot.feign.facade;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.alinesno.cloud.base.boot.feign.dto.ManagerDepartmentDto;
import com.alinesno.cloud.common.facade.feign.IBaseFeign;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;

/**
 * <p>  请求客户端 </p>
 *
 * @author LuoAnDong
 * @since 2019-03-24 13:24:58
 */
@FeignClient(name="alinesno-cloud-base-boot" , path="managerDepartment")
public interface ManagerDepartmentFeigin extends IBaseFeign<ManagerDepartmentDto> {

	@PostMapping("findAllWithApplication")
	List<ManagerDepartmentDto> findAllWithApplication(@RequestBody RestWrapper restWrapper);

}
