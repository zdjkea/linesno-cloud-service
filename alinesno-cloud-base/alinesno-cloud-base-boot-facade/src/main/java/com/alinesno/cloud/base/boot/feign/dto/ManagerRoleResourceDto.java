package com.alinesno.cloud.base.boot.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:00:40
 */
@SuppressWarnings("serial")
public class ManagerRoleResourceDto extends BaseDto {

    /**
     * 角色id
     */
	private String roleId;
	
    /**
     * 资源id
     */
	private String resourceId;
	
    /**
     * 资源类型(resource/action)
     */
	private String resourceType;
	
    /**
     * 角色类型(role角色|tenant商户|account账户)
     */
	private String roleType;
	


	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

}
