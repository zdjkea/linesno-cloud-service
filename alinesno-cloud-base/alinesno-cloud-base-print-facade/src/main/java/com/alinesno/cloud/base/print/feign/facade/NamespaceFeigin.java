package com.alinesno.cloud.base.print.feign.facade;

import org.springframework.cloud.openfeign.FeignClient;
import com.alinesno.cloud.common.facade.feign.IBaseFeign;
import com.alinesno.cloud.base.print.feign.dto.NamespaceDto;

/**
 * <p>  请求客户端 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-07 21:26:00
 */
@FeignClient(name="alinesno-cloud-base-print" , path="namespace")
public interface NamespaceFeigin extends IBaseFeign<NamespaceDto> {

}
