package com.alinesno.cloud.base.notice.feign.facade;

import org.springframework.cloud.openfeign.FeignClient;
import com.alinesno.cloud.common.facade.feign.IBaseFeign;
import com.alinesno.cloud.base.notice.feign.dto.SmsFailDto;

/**
 * <p>  请求客户端 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-08 06:27:10
 */
@FeignClient(name="alinesno-cloud-base-notice" , path="smsFail")
public interface SmsFailFeigin extends IBaseFeign<SmsFailDto> {

}
