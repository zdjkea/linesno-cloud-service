package com.alinesno.cloud.base.logger.agent.aop;

import java.io.Serializable;
import java.util.Date;

/**
 * 当前用户操作监控实体
 * 
 * @author LuoAnDong
 * @since 2019年4月8日 下午8:50:57
 */
public class AccountMonitorBean implements Serializable {

	private static final long serialVersionUID = -6309732882044872298L;

	private String id ; 
	private String operation;
	private long time;
	private String method;
	private String params;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getAgent() {
		return agent;
	}
	public void setAgent(String agent) {
		this.agent = agent;
	}
	private String ip; //服务器ip
	private String url ;  //请求链接
	private String agent ; // 浏览器信息
	
	private Date createTime;

	private String accountId ;
	private String loginName ;
	private String accountName;
	private String rolePower ;
	private String applicationId; // 所属应用
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getRolePower() {
		return rolePower;
	}
	public void setRolePower(String rolePower) {
		this.rolePower = rolePower;
	}
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getParams() {
		return params;
	}
	public void setParams(String params) {
		this.params = params;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	
}