package com.alinesno.cloud.base.boot.web.module.platform;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alinesno.cloud.common.web.base.controller.BaseController;

/**
 * 应用管理 
 * @author LuoAnDong
 * @since 2018年12月7日 下午10:58:20
 */
@Controller
@RequestMapping("boot/platform/search")
public class SearchController extends BaseController {

	@RequestMapping("/list")
    public void list(){
    }

}
