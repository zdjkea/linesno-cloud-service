package com.alinesno.cloud.platform.stack.repository;

import static org.junit.Assert.fail;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alinesno.cloud.operation.cmdb.repository.EmailRepository;

/**
 * 邮件单元测试
 * @author LuoAnDong
 * @since 2018年8月29日 下午9:16:19
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class EmailRepositoryTest {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass()) ; 

	@Autowired
	private EmailRepository emailRepository ; 

	@Test
	public void testFindByEmail() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllLimit() {
	}
	
	@Test
	public void testFindAllByRendStatus() {
	}
	
	@Test
	public void testCountByRendStatusAndDate() {
		int count = emailRepository.countByRendStatusAndDate(1, "2018-08-29") ; 
		logger.debug("count = {}" , count);
	}

}
