package com.alinesno.cloud.operation.cmdb.third.wechat;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;

/**
 * 微信配置
 * @author LuoAnDong
 * @since 2018年8月5日 下午9:37:25
 */
@Component
public class WechatConfig {

	private static WxMpInMemoryConfigStorage config = new WxMpInMemoryConfigStorage();
	public static final Object WX_OAUTH2_TOKEN = "wx_auth2_token" ; 
	public static Map<String , String> tokenMap = new HashMap<String , String>() ; 

	@Value("${wechat.app.id}")  
	private String appId ; 
	
	@Value("${wechat.app.secret}")  
	private String appSecret ; 
	
	@Value("${wechat.token}")  
	private String token ; 
	
	@Value("${wechat.aes-key}")  
	private String aesKey ; 
	
	private static final String TEXT_COLDE = "#333333" ; 
	
	/**
	 * 实时交易提醒
	 */
	@Value("${wechat.templates.t1}")  
	private String messageTemplate1 ; 

	/**
	 * 反馈处理结果通知
	 */
	@Value("${wechat.templates.t2}")  
	private String messageTemplate2 ; 
	
	/**
	 * 反馈处理结果通知
	 */
	@Value("${wechat.templates.feekback}")  
	private String noticeFeelbackTemplate3 ; 

//	
	/**
	 * 新订单通知<br/>
	 *  {{first.DATA}}<br/>
		订单编号：{{keyword1.DATA}}<br/>
		客户昵称：{{keyword2.DATA}}<br/>
		订单价格：{{keyword3.DATA}}<br/>
		订单标题：{{keyword4.DATA}}<br/>
		订单截止时间：{{keyword5.DATA}}<br/>
		{{remark.DATA}}
	 */
	@Value("${wechat.templates.order-new}")  
	private String msgNewOrder; 

	/**
	 * 订单修改通知<br/>
	 * {{first.DATA}}<br/>
		订单状态：{{keyword1.DATA}}<br/>
		时间：{{keyword2.DATA}}<br/>
		{{remark.DATA}}<br/>
	 */
	@Value("${wechat.templates.order-change}")  
	private String msgChangeOrder; 
	
	private WxMpService wxService = null ;  

	/**
	 * 基础配置
	 */
	public WechatConfig() {
	}
	
	public WxMpService getInstance() {
		if(wxService == null) {
			wxService  = new WxMpServiceImpl() ; 
			config.setAppId(appId); // 设置微信公众号的appid
			config.setSecret(appSecret); // 设置微信公众号的app corpSecret
			config.setToken(token); // 设置微信公众号的token
			config.setAesKey(aesKey); // 设置微信公众号的EncodingAESKey
			wxService.setWxMpConfigStorage(config);
		}
		return wxService ; 
	}

	/**
	 * 生成订单通知
	 * @param url 发送的url
	 * @param user 发送的用户
	 * @param firstText 说明
	 * @param orderId 订单号
	 * @param customerName 客户名称
	 * @param orderTitle 订单名称
	 * @param totalPay 费用
	 * @param endTime 结束时间 
	 * @param remark 备注
	 * @return
	 * @throws WxErrorException
	 */
	public WxMpTemplateMessage sendNewOrder(String url , String user , 
			String firstText , 
			String orderId , 
			String customerName , 
			String orderTitle ,
			String totalPay ,
			String endTime , 
			String remark 
			) throws WxErrorException {
		WxMpTemplateMessage templateMessage = WxMpTemplateMessage.builder()
				.toUser(user)
				.templateId(msgNewOrder)
				.url(url)
				.build();
		
		templateMessage.addData(new WxMpTemplateData("first", firstText, TEXT_COLDE));
		templateMessage.addData(new WxMpTemplateData("keyword1", orderId, TEXT_COLDE));
		templateMessage.addData(new WxMpTemplateData("keyword2", customerName, TEXT_COLDE));
		templateMessage.addData(new WxMpTemplateData("keyword3", totalPay , TEXT_COLDE));
		templateMessage.addData(new WxMpTemplateData("keyword4", orderTitle, TEXT_COLDE));
		if(StringUtils.isNoneBlank(endTime)) {
			templateMessage.addData(new WxMpTemplateData("keyword5", endTime , TEXT_COLDE));
		}
		templateMessage.addData(new WxMpTemplateData("remark",  remark)) ; 
		
		//"订单号:【" + orderInfo.getOrderId() + "】\r\n状态:【"+OrderStatusEnum.getStatus(orderInfo.getSendStatus()).getText()+"】", TEXT_COLDE));

		return templateMessage ; 
	}

	/**
	 * 订单状态修改通知
	 * @param url 返回的url
	 * @param user 用户
	 * @param firstText 说明 
	 * @param orderStatus 订单状态
	 * @param changeTime 状态修改时间 
	 * @param remark 备注
	 * @return
	 * @throws WxErrorException
	 */
	public WxMpTemplateMessage sendChangOrder(String url , String user , 
			String firstText , 
			String orderStatus,  
			String changeTime ,  
			String remark) throws WxErrorException {
		WxMpTemplateMessage templateMessage = WxMpTemplateMessage.builder()
				.toUser(user)
				.templateId(msgChangeOrder)
				.url(url)
				.build();
		
		templateMessage.addData(new WxMpTemplateData("first", firstText, TEXT_COLDE));
		templateMessage.addData(new WxMpTemplateData("keyword1", orderStatus, TEXT_COLDE));
		templateMessage.addData(new WxMpTemplateData("keyword2", changeTime , TEXT_COLDE));
		templateMessage.addData(new WxMpTemplateData("remark",  remark)) ; 
		
		return templateMessage ; 
	}
	

	/**
	 * 发送通知给业务员
	 * @return 
	 * @throws WxErrorException 
	 */
	public WxMpTemplateMessage sendTemplate(String url , String templateId ,  String user) throws WxErrorException {
		WxMpTemplateMessage templateMessage = WxMpTemplateMessage.builder()
				.toUser(user)
				.templateId(templateId)
				.url(url)
				.build();
		return templateMessage ; 
	}
	
	/**
	 * 反馈处理结果通知
	 * @param url 链接
	 * @param user 申请处理
	 * @param firstText 申请说明 
	 * @param dealMan 处理人员
	 * @param date 处理时间 
	 * @param result 处理结果 
	 * @return remark 备注
	 * @throws WxErrorException
	 */
	public WxMpTemplateMessage sendFeelbackNotice(String url , 
			String user , 
			String firstText , 
			String dealMan ,  //处理人员
			String date , 	  //处理时间 
			String result ,  	  //处理结果
			String remark  //备注
			) throws WxErrorException {
		WxMpTemplateMessage templateMessage = WxMpTemplateMessage.builder()
				.toUser(user)
				.templateId(noticeFeelbackTemplate3)
				.url(url)
				.build();
		
		templateMessage.addData(new WxMpTemplateData("first", firstText, TEXT_COLDE));
		templateMessage.addData(new WxMpTemplateData("keyword1", dealMan, TEXT_COLDE));
		templateMessage.addData(new WxMpTemplateData("keyword2", date , TEXT_COLDE));
		templateMessage.addData(new WxMpTemplateData("keyword3", result , TEXT_COLDE));
		templateMessage.addData(new WxMpTemplateData("remark",  remark)) ; 
		
		return templateMessage ; 
	}

}

























