package com.alinesno.cloud.operation.cmdb.third.wechat.controller;

import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;

/**
 * 微信消息基类
 * @author LuoAnDong
 * @since 2018年10月11日 上午8:59:12
 */
public abstract class WechatMessageBase {

	/**
	 * 微信消息处理类
	 * @param message
	 * @param content 
	 * @param toUser 
	 * @param fromUser 
	 * @return
	 */
	public abstract Object handlerMessage(WxMpXmlMessage message, String fromUser, String toUser, String event ,  String content)  ; 
	
}
