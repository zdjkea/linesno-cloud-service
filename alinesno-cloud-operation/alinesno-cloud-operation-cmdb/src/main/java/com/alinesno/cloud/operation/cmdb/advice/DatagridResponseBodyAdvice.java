package com.alinesno.cloud.operation.cmdb.advice;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alinesno.cloud.operation.cmdb.common.constants.MasterMachineEnum;
import com.alinesno.cloud.operation.cmdb.entity.MasterMachineEntity;
import com.alinesno.cloud.operation.cmdb.repository.MasterMachineRepository;
import com.alinesno.cloud.operation.cmdb.web.bean.JqDatatablesPageBean;

/**
 * 数据切面
 * @author LuoAnDong
 * @since 2018年9月23日 下午6:28:15
 */
@Order(0)
@ControllerAdvice
public class DatagridResponseBodyAdvice implements ResponseBodyAdvice<JqDatatablesPageBean> {

	// 日志记录
	private final static Logger logger = LoggerFactory.getLogger(DatagridResponseBodyAdvice.class);

	@Autowired
	private MasterMachineRepository masterMachineRepository ; 

	/**
	 * 拦截数据
	 */
	@Override
	public JqDatatablesPageBean beforeBodyWrite(JqDatatablesPageBean baseGridData, MethodParameter methodParameter, MediaType mediaType, Class<? extends HttpMessageConverter<?>> classes, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
		Object dataObject = baseGridData.getData();
		
		if (dataObject instanceof List) {
		
			JSONArray jsonArr = JSONArray.parseArray(JSONArray.toJSONString(dataObject,SerializerFeature.WriteNullStringAsEmpty));	
			List<JSONObject> jsonObjectList = new ArrayList<JSONObject>();
			
			if (!jsonArr.isEmpty()) {
				
				for (int j = 0; j < jsonArr.size(); j++) {
					JSONObject jsonBean = jsonArr.getJSONObject(j);
						
					// 机房代码转换
					String masterCode = jsonBean.getString("masterCode");
					if(StringUtils.isNoneBlank(masterCode)) {
						MasterMachineEntity school = masterMachineRepository.findByMasterCode(masterCode) ; 
						if(school != null) {
							jsonBean.put("masterCode_label", school.getMasterName());
						}
					}else {
						jsonBean.put("masterCode_label", MasterMachineEnum.ERROR.getText());
					}
					
					jsonObjectList.add(jsonBean) ; 
				}
			}
			
			baseGridData.setData(jsonObjectList); 
		}

		return baseGridData;
	}

	@Override
	public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> classes) {
		ConvertCode convertCode = methodParameter.getMethod().getAnnotation(ConvertCode.class);

		if (logger.isDebugEnabled()) {
			logger.debug("convertCode = {}", convertCode);
			Annotation[] annotations = methodParameter.getMethodAnnotations();
			for (Annotation a : annotations) {
				logger.debug("annotation = {}", a);
			}
		}

		return convertCode == null ? false : true;
	}

}