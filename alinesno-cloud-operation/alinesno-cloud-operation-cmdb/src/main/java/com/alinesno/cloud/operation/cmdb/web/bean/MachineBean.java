package com.alinesno.cloud.operation.cmdb.web.bean;

/**
 * 商品列表
 * @author LuoAnDong
 * @since 2018年9月9日 上午11:08:18
 */
public class MachineBean {

	private String machineName ; //虚拟机名称
	private String machinePrices ; //虚拟机价格
	private int machineNum ;  //虚拟机数量 
	private String machineDesc ; //虚拟机描述 
	private String machineStatus ; //虚拟机状态(0正常|1下架)
	private String machinePic ; //虚拟机图片
	private int discount ; // 打折(按百分比)
	private String userId ; // 所属用户
	private String machineIp ; //虚拟机IP
	private String machineOpenPort ; //开放端口
	private String machineRootname ; // root账户名
	private String machinePwd ; //虚拟机密码
	private String masterMachine ; //所属主机 
	
	public String getMasterMachine() {
		return masterMachine;
	}
	public void setMasterMachine(String masterMachine) {
		this.masterMachine = masterMachine;
	}
	public String getMachineName() {
		return machineName;
	}
	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}
	public String getMachinePrices() {
		return machinePrices;
	}
	public void setMachinePrices(String machinePrices) {
		this.machinePrices = machinePrices;
	}
	public int getMachineNum() {
		return machineNum;
	}
	public void setMachineNum(int machineNum) {
		this.machineNum = machineNum;
	}
	public String getMachineDesc() {
		return machineDesc;
	}
	public void setMachineDesc(String machineDesc) {
		this.machineDesc = machineDesc;
	}
	public String getMachineStatus() {
		return machineStatus;
	}
	public void setMachineStatus(String machineStatus) {
		this.machineStatus = machineStatus;
	}
	public String getMachinePic() {
		return machinePic;
	}
	public void setMachinePic(String machinePic) {
		this.machinePic = machinePic;
	}
	public int getDiscount() {
		return discount;
	}
	public void setDiscount(int discount) {
		this.discount = discount;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getMachineIp() {
		return machineIp;
	}
	public void setMachineIp(String machineIp) {
		this.machineIp = machineIp;
	}
	public String getMachineOpenPort() {
		return machineOpenPort;
	}
	public void setMachineOpenPort(String machineOpenPort) {
		this.machineOpenPort = machineOpenPort;
	}
	public String getMachineRootname() {
		return machineRootname;
	}
	public void setMachineRootname(String machineRootname) {
		this.machineRootname = machineRootname;
	}
	public String getMachinePwd() {
		return machinePwd;
	}
	public void setMachinePwd(String machinePwd) {
		this.machinePwd = machinePwd;
	}
	

}
