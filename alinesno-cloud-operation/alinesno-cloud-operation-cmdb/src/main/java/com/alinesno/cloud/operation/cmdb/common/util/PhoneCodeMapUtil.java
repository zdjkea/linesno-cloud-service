package com.alinesno.cloud.operation.cmdb.common.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;


/**
 * 验证码工具类
 * @author LuoAnDong
 * @since 2018年8月5日 下午4:49:50
 */
public class PhoneCodeMapUtil {

	private static final Logger logger = LoggerFactory.getLogger(PhoneCodeMapUtil.class) ; 
	private static Map<String , CodeBean> map = new ConcurrentHashMap<String , CodeBean>() ; 
	public static int OUTTIME_SECOND =  2*60 ; //验证码有效期120秒

	/**
	 * key生成规则:手机号
	 * @param code
	 */
	public static void put(String phone , CodeBean code) {
		map.put(phone , code) ; 
	}

	/**
	 * 获取key
	 * @param phone
	 * @return
	 */
	public static String get(String phone) {
		if(StringUtils.isBlank(phone)) {
			return null ;
		}
		boolean isOuttime = validateCodeByPhone(phone) ; 
		if(isOuttime) {
			return null ; 
		}
		CodeBean codeBean = map.get(phone); 
		return codeBean.getCode() ; 
	}

	/**
	 * 验证此手机号的验证码是否过期
	 * @param phone
	 * @return true过期|false未过期
	 */
	public static boolean validateCodeByPhone(String phone) {
		Assert.notNull(phone , "手机号不能为空.");
		CodeBean codeBean = map.get(phone) ; 
		if(codeBean != null) {
			long createTime = codeBean.getCreateTime() ; 
			String code = codeBean.getCode() ;
			
			if(StringUtils.isNotBlank(code)) { 
				long currentTime = System.currentTimeMillis() ;  
				
				long idleTime = currentTime - createTime ;  // 空隙时间 
				logger.debug("phone:[{}] , code:[{}] , idea time:[{}]" , phone , code , idleTime);
			
				if(idleTime > OUTTIME_SECOND*1000) {
					//过期则删除key 
					map.remove(phone) ; 
					return true ; 
				}else {
					return false ; 
				}
			}
		}
		return true ; 
	}

}
