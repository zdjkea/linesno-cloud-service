package com.alinesno.cloud.operation.cmdb.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 账户资源
 * @author LuoAnDong
 * @since 2018年9月23日 上午6:54:38
 */
@SuppressWarnings("serial")
@Entity
@Table(name="cmdb_account_resource")
public class AccountResourceEntity extends BaseEntity {

	private String resourceId ; //资源id
	private String accountId ; //账户id 
	private String dataScope ; //数据范围(all全部数据|area区域数据)
	
	public String getDataScope() {
		return dataScope;
	}
	public void setDataScope(String dataScope) {
		this.dataScope = dataScope;
	}
	public String getResourceId() {
		return resourceId;
	}
	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	
}
