package com.alinesno.cloud.operation.cmdb.repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.alinesno.cloud.operation.cmdb.entity.ServerEntity;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface BudinessServerRepository extends BaseJpaRepository<ServerEntity, String> {

	/**
	 * 分页查询机房区域信息
	 * @param shoolCode
	 * @param of
	 * @return
	 */
	Page<ServerEntity> findAllByMasterCode(String shoolCode, Pageable pageable);

	/**
	 * 查询服务状态正常
	 * @param masterCode
	 * @param normal
	 * @param sort
	 * @return
	 */
	Iterable<ServerEntity> findAllByMasterCodeAndHasStatus(String masterCode, int hasStatus , Sort sort);

	/**
	 * 通过IP查询物理主机
	 * @param servername
	 * @return
	 */
	ServerEntity findByServerName(String serverName);

}