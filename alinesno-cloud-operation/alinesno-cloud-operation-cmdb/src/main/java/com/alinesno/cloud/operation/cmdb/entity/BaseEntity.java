package com.alinesno.cloud.operation.cmdb.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.DiscriminatorOptions;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 实体对象基类,定义基本属性
 *
 * @author LuoAnDong
 * @date 2017年8月4日
 */
@EntityListeners(AuditingEntityListener.class)
@DiscriminatorColumn(name = "tenantId") // 添加多租户支持
@DiscriminatorOptions(force = true)
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
@MappedSuperclass
public class BaseEntity implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "snowFlakeId")
	@GenericGenerator(name = "snowFlakeId", strategy = "com.alinesno.cloud.operation.cmdb.common.id.SnowflakeId")
	@Column(unique = true, nullable = false, length = 36)
	private String id; // 唯一ID号
	private String fieldProp; // 字段属性

	@CreatedDate
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date addTime; // 添加时间

	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date deleteTime; // 删除时间

	@LastModifiedDate
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime; // 更新时间

	private Integer hasDelete = 0; // 是否删除(1删除|0正常|null正常)
	private Integer hasStatus = 0; // 状态(0启用|1禁用)
	private String deleteManager; // 删除的人

	@Column(length = 36)
	private String applicationId = "0"; // 所属应用

	@Column(length = 36)
	private String tenantId = "0"; // 所属租户

	private String masterCode; // 所属机房

	public String getMasterCode() {
		return masterCode;
	}

	public void setMasterCode(String masterCode) {
		this.masterCode = masterCode;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public int getHasStatus() {
		return hasStatus;
	}

	public void setHasStatus(int hasStatus) {
		this.hasStatus = hasStatus;
	}

	public String getDeleteManager() {
		return deleteManager;
	}

	public void setDeleteManager(String deleteManager) {
		this.deleteManager = deleteManager;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFieldProp() {
		return fieldProp;
	}

	public void setFieldProp(String fieldProp) {
		this.fieldProp = fieldProp;
	}

//	public int isHasDelete() {
//		return hasDelete;
//	}
//
//	public void setHasDelete(int hasDelete) {
//		this.hasDelete = hasDelete;
//	}

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getAddTime() {
		return addTime;
	}

	public int getHasDelete() {
		return hasDelete;
	}

	public void setHasDelete(int hasDelete) {
		this.hasDelete = hasDelete;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getDeleteTime() {
		return deleteTime;
	}

	public void setDeleteTime(Date deleteTime) {
		this.deleteTime = deleteTime;
	}

	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

}
