package com.alinesno.cloud.operation.cmdb.entity;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * 资产管理管理(不包含服务器)
 * 
 * @author LuoAnDong
 * @since 2018年8月5日 下午12:20:30
 */
@SuppressWarnings("serial")
@Entity
@Table(name="cmdb_asserts")
public class AssertsEntity extends BaseEntity {

	@Excel(name = "资产类型")
	private String assertType ; //资产类型
	
	@Excel(name = "资产名称")
	private String assertName ; //资产名称 
	
	@Excel(name = "资产编号")
	private String assertNum ; //资产编号
	
	@Excel(name = "资产标签")
	private String assertTags ; //资产标签 
	
	@Excel(name = "资产业务线")
	private String businessType ; //资产业务线
	
	@Excel(name = "资产管理员")
	private String managerName ; //资产管理员
	
	@Excel(name = "资产使用人员")
	private String assertMan ; //资产使用人员
	
	@Lob
	@Excel(name = "资产详情")
	private String assertDetail ; // 资产详情(记录资产的情况)
	
	public String getAssertMan() {
		return assertMan;
	}
	public void setAssertMan(String assertMan) {
		this.assertMan = assertMan;
	}
	public String getAssertDetail() {
		return assertDetail;
	}
	public void setAssertDetail(String assertDetail) {
		this.assertDetail = assertDetail;
	}
	public String getAssertType() {
		return assertType;
	}
	public void setAssertType(String assertType) {
		this.assertType = assertType;
	}
	public String getAssertName() {
		return assertName;
	}
	public void setAssertName(String assertName) {
		this.assertName = assertName;
	}
	public String getAssertNum() {
		return assertNum;
	}
	public void setAssertNum(String assertNum) {
		this.assertNum = assertNum;
	}
	public String getAssertTags() {
		return assertTags;
	}
	public void setAssertTags(String assertTags) {
		this.assertTags = assertTags;
	}
	public String getBusinessType() {
		return businessType;
	}
	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}
	public String getManagerName() {
		return managerName;
	}
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

}
